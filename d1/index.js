console.log(`Hello World 👋 🌏`)

// Array Methods
// Javascript has built-in functions and methods for arrays
// This allows us to manipulate and access array items

// Mutator Methods
/*
    - Mutator Methods are functions that "mutate" or change an array after they are created
    - These methods manipulate the original array peforming various tasks such as adding and removing elements
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit']

// .push()
/* 
  Adds an element in the end of an array and returns the array length

  Syntax:
  arrayName.push()
*/
console.log('Current Array: ')
console.log(fruits)

let fruitsLength = fruits.push('Mango')
console.log(fruitsLength)
console.log('Mutated Array From .push(): ')
console.log(fruits)

fruits.push('Avocado', 'Guava')
console.log('Mutated Array From .push(): ')
console.log(fruits)

// pop

/*
  Removes the last element in an array and returns the removed element

  Syntax:
  arrayName.pop()
*/

let removedFruit = fruits.pop()
console.log(removedFruit)
console.log('Mutated Array From pop(): ')
console.log(fruits)

let ghostFighters = ['Eugene', 'Dennis', 'Alfred', 'Taguro']

/*
  Mini Activity #1
  - Create a function which will remove the last person in the array and log the ghostFighters array in the console
  - Send an screenshot of your output
*/

function removeGhostFighter() {
  ghostFighters.pop()
}

removeGhostFighter()
console.log(ghostFighters)

// unshift()
/* 
  Adds one or more elements at the beginner of an array

  Syntax:
  arrayName.unshift(elementA)
  arrayName.unshift(element_A, element_B)
*/

fruits.unshift('Lime', 'Banana')
console.log('Mutated Array From .unshift(): ')
console.log(fruits)

// shift()
/*
  Removes an element at the beginning of an array and returns the removed element

  Syntax:
  arrayName.shift()
*/
let anotherFruit = fruits.shift()
console.log(anotherFruit)
console.log('Mutated Array From .shift(): ')
console.log(fruits)

// splice()
/*
  simultaneously removes lements from a specified number and adds the element

  Syntax:
  arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 0, 'Lime', 'Cherry')
console.log('Mutated Array From .splice(): ')
console.log(fruits)

// sort
fruits.sort()
console.log('Mutataed array from sort method:')
console.log(fruits)

// reverse
fruits.reverse()
console.log('Mutataed array from reverse method:')
console.log(fruits)

//Non-Mutator Methods
/*
		Non-Mutator Methods 
    - functions that do not modify or change an array after they are created
		- these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
	*/

let countries = ['US', 'PH', 'CAN', 'TH', 'PH', 'FR', 'DE']

//! You need to review this
// indexOf()

/*
  Returns the index of the first matching element found in an array
  – if no match is found, the result is -1
  - the search process will be done from first element processing to the last element

  syntax:
  arrayName.indexOf(searchValue)
  arrayName.indexOf(searchValue, fromIndex)
*/

let firstIndex = countries.indexOf('PH')
console.log(`Result of .indexOf(): ${firstIndex}`)
let firstExample = countries.indexOf('ph')
console.log(`Result of .indexOf(): ${firstExample}`)
let invalidCountry = countries.indexOf('BR')
console.log(`Result of .indexOf(): ${invalidCountry}`)

// lastIndexOf()
/*
  - returns the index of the last matching element found in an array
  - the search process will be done from last element processing to the last element

  syntax:
  arrayName.indexOf(searchValue)
  arrayName.indexOf(searchValue, fromIndex)
*/

let lastIndex = countries.lastIndexOf('PH', 3)
console.log(`Result of .lastindexOf(): ${lastIndex}`)

//! you need to review this
// slice()
/*
  – portions/slices elements from an array and returns a new array

  syntax:
  arrayName.slice(startingIndex)
  arrayName.slice(startingIndex, endingIndex)

*/

console.log(countries)
// ['US', 'PH', 'CAN', 'TH', 'PH', 'FR', 'DE']
let slicedArrayA = countries.slice(2)
console.log(`Result from .slice():`)
console.log(slicedArrayA)
console.log(countries)

let slicedArrayB = countries.slice(2, 4)
console.log(`Result from .slice():`)
console.log(slicedArrayB)

let slicedArrayC = countries.slice(-3)
console.log(`Result from .slice():`)
console.log(slicedArrayC)

// toString()
/*
      returns an array as a string separated by commas

      Syntax:
      arrayName.toString()
*/

let stringArray = ghostFighters.toString()
console.log(`Result from .toString:`)
console.log(stringArray)
console.log(typeof stringArray)

// concat
/*
combine two arrays and returns the combined result

Syntax:
arrayA.concat(arrayB)
arrayA.concat(elementA)
*/

let taskArrayA = ['drink HTML', 'eat JavaScript']
let taskArrayB = ['inhale CSS', 'breathe React']
let taskArrayC = ['get git', 'be NODE']

let tasks = taskArrayA.concat(taskArrayB)
console.log('Result from concat method:')
console.log(tasks)

// combine multiple arrays
console.log(`Result from .concat():`)
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)
console.log(allTasks)

// combine arrays with elements
let combinedTasks = taskArrayA.concat('smell Express', 'throw MongoDB')
console.log(`Result from .concat()`)
console.log(combinedTasks)

// join
/*
  Returns an array as a string separated by a specific separator

  Syntax:
  arrayName.join('separatorString')
*/
let users = ['John', 'Jane', 'Joe', 'Robert', 'Neil']
console.log(users.join())
console.log(users.join(' '))
console.log(users.join(' - '))
console.log(users.join(' * '))
console.log(users.join(', '))

//Iteration Method

/*
		Iteration methods
    - loops designed to perform repetitive tasks on arrays
		- loops over all items in an array
		Useful for mani[ulating array data resulting in complex tasks
*/

// forEach
/*
      similar to for-loop that iterates on each array element
      – for each item in the array, the anonymous function passed in the forEach() method will be run
      – anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
      - variable names for arrays are normally written in the plural form of the data stored in an array
      - it's common practice to use the singular form of the array content for parameter names used in array loops
      – forEach() does not return anything
*/

allTasks.forEach(function (task) {
  console.log(task)
})

/*
  Mini Activity #2
  - create a function that can display the ghostFighters one by one in our console
  - Invoke the function
  - send a screenshot
*/

//! wrong naming condition! Singular Form!
function displayGhostFighters() {
  ghostFighters.forEach(function (ghostFighter) {
    console.log(ghostFighter)
  })
}
displayGhostFighters()

// user forEach with conditional statements
let filteredTasks = []
allTasks.forEach(function (task) {
  // console.log(task)

  if (task.length > 10) {
    console.log(`${task} my length is greater than ten`)
    filteredTasks.push(task)
  }
})
console.log(`Result of filteredTasks:`)
console.log(filteredTasks)

// map()
/*
  iterates on each element and returns NEW ARRAY with different values depending on the result of the functions operation
*/

let numbers = [1, 2, 3, 4, 5]
let numberMap = numbers.map(function (number) {
  return number * number
})

console.log(`Original Array:`)
console.log(numbers)
console.log(`Result of .map():`)
console.log(numberMap)

// map() vs forEach()
let numberForEach = numbers.forEach(function (number) {
  return number
})

console.log(numberForEach)

// every()
/*
  check if all elements in an array meet the given condition
*/

let allValid = numbers.every(function (number) {
  return number < 3
})
console.log(allValid)

// some()
/*
  check if at least one element meet the given condition
*/

let someValid = numbers.some(function (number) {
  return number < 2
})
console.log(someValid)

// filter()
let filterValid = numbers.filter(function (number) {
  return number < 3
})
console.log(filterValid)

let nothingFound = numbers.filter(function (number) {
  return (number = 0)
})
console.log(nothingFound)

// filtering using forEach()
let filteredNumbers = []
numbers.forEach(function (number) {
  if (number < 3) 
    filteredNumbers.push(number) //prettier-ignore
})
console.log(filteredNumbers)

// includes()
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor', 'AAAA']
let productFound = products.includes('Mouse')
console.log(productFound)

let productNotFound = products.includes('headset')
console.log(productNotFound)

// Method chaining
let filteredProducts = products.filter(function (product) {
  return product.toLowerCase().includes('a')
})
console.log(filteredProducts)

// reduce
/*
  syntax:
  let/const resultArray = arrayname.reduce(function(accumulator, currentValue){
    return expression/operator
  })
*/
console.log(numbers)
let iteration = 0

let reducedArray = numbers.reduce(function (x, y) {
  console.warn(`Current iteration ${++iteration} `)
  console.log(`accumulator: ${x}`)
  console.log(`current value: ${y}`)
  return x + y
})

console.log(`Result of reduce method: ${reducedArray}`)

let iterationStr = 0
let list = ['Hello', 'Again', 'World']
let reducedJoin = list.reduce(function (x, y) {
  console.warn(`Current iteration ${++iterationStr} `)
  console.log(`accumulator: ${x}`)
  console.log(`current value: ${y}`)
  return `${x} ${y}`
})
console.log(`${reducedJoin}`)

/*
		Mini Activity #3
		--Create an addTrainer function that will enable us to add a trainer in the contacts array
		--This function should be able to receive a string
		--Determine if the added trainer already exists in the contacts array:
		--if it is, show an alert saying "Already added in the Match Call(Contacts)"
		--if it is not, add the trainer in the contacts array and show an alert saying "Registered!"
		--invoke and add a trainer in the browser's console
		--in the console, log the contacts array
	*/
